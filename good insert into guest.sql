use V_I_module_3
go

insert into [V_I_module_3].[dbo].[guest]
(
       [name]
      ,[room_id]
      ,[arrival_date]
      ,[departure_time]
      ,[email]
      ,[phone_number]
      ,[children]
      ,[insert_date]
      ,[update_date]
)
values
(
       'Ilkiv Volodymyr'
      ,1
      ,'2018-07-15 12:00:00'
      ,'2018-07-16 12:00:00'
      ,'lkivvova@gmail.com'
      ,'0974862676'
      ,null
      ,null
      ,null
)
go

select * from [dbo].[guest]
go